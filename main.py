import argparse
import app.app as app
import app.experiments as exp
import tests.tests as test

description = 'Medifor reasoning engine for detection and localization.'
parser = argparse.ArgumentParser(description=description)
parser.add_argument('-r', '--run', help='Run reasoning engine',
        action='store_true')
parser.add_argument('-s', '--score_local', help='Do local mask scoring',
        action='store_true')
parser.add_argument('--combo_exp', help='Combinations Experiment',
        action='store_true')
parser.add_argument('-t', '--tests', help='Run unit tests',
        action='store_true')
args = parser.parse_args()

app.configure_settings()

if args.run:
    app.run(model_type='rf', use_image_quality=True, train_sets='4',
        val_sets='2', test_sets='2', algorithms='all', gs_folds=10,
        evaluation='cc', explain_preds=0, explain_correct=False,
        explain_features=False, normalization='minmax', task='global',
        verbose=2, param_search='high')

elif args.score_local:
    app.score_local(train_sets='1', test_sets='2')

elif args.combo_exp:
    cc_dsets = [('2', '4', '1'), ('4', '2', '1'), ('24', None, '1'),
            ('1', '4', '2'), ('4', '1', '2'), ('14', None, '2'),
            ('1', '24', '3'), ('2', '14', '3'), ('4', '12', '3'),
            ('12', '4', '3'), ('14', '2', '3'), ('24', '1', '3'),
            ('124', None, '3'), ('1', '2', '4'), ('2', '1', '4'),
            ('12', None, '4')]
    cc_s = '\ntrain: %s,  test: %s,  model_type: %s,  '
    cc_s += 'image_qual: %r,  indicators: %s'

    cv_dsets = [('1', None, '1'), ('2', None, '1'), ('4', None, '1')]
    cv_s = '\ndataset: %s,  model_type: %s,  image_qual: %r,  indicators: %s'

    exp.combo_exp(dsets=cc_dsets, s=cc_s, evaluation='cc')

elif args.tests:
    test.run_tests()

import unittest
from . import test_util


def run_tests():
    suites = []

    suites.append(test_util.test_suite())

    tests = unittest.TestSuite(suites)
    unittest.TextTestRunner().run(tests)

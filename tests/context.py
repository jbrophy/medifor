import os
import sys
one_up = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, one_up)

from app.utility import util

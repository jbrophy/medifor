import unittest
import numpy as np
from .context import util as ut


class UtilTestCase(unittest.TestCase):

    def test_div0_denom_zero(self):
        result = ut.div0(10, 0)
        assert result == 0

    def test_div0_num_zero(self):
        result = ut.div0(0, 10)
        assert result == 0

    def test_div0_both_zero(self):
        result = ut.div0(0, 0)
        assert result == 0

    def test_div0_odd(self):
        result = ut.div0(10, 3)
        assert np.isclose(result, 3.33333)

    def test_div0_even(self):
        result = ut.div0(50, 25)
        assert result == 2.0


def test_suite():
    suite = unittest.TestLoader().loadTestsFromTestCase(UtilTestCase)
    return suite

if __name__ == '__main__':
    unittest.main()

import os
import random
import time as t
import numpy as np
import pandas as pd
import skimage.io as skio
from . import data as d
from ..utility import util as ut
from ..interpretability import explainer as exp
from PIL import Image
from operator import itemgetter
from sklearn.model_selection import GroupKFold
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import PredefinedSplit


class Task:
    """Abstract class defining methods that must be implemented."""

    def cross_compare(self):
        raise NotImplementedError('Method not implemented')

    def cross_val(self):
        raise NotImplementedError('Method not implemented')

    def train_test(self):
        raise NotImplementedError('Method not implemented')

    def _drop_missing_data_from_df(self, df):
        df = df.dropna(axis=1, how='all')
        return df


class Global(Task):

    out_d = 'output/global/'

    # public
    def cross_compare(self, data, param_search=None, gs_folds=2, verbose=1,
            explain_preds=0, explain_correct=True, explain_features=False,
            algorithms='base', model_type='xgb', normalization='minmax',
            use_image_quality=True, train_col='both', val_col='both',
            test_col='both', evaluation='cc'):

        train_dsets, tr_name = data['train']
        test_dsets, te_name = data['test']

        val_feats = None
        norm_val_df = None
        if 'val' in data.keys():
            val_dsets, va_name = data['val']
            val_df, val_feats = self._prepare_data(val_dsets,
                dset='val', algorithms=algorithms,
                use_image_quality=use_image_quality, train_col=train_col,
                val_col=val_col, test_col=test_col, evaluation=evaluation)

        # data preparation
        train_df, train_feats = self._prepare_data(train_dsets, dset='train',
            algorithms=algorithms, use_image_quality=use_image_quality,
            train_col=train_col, val_col=val_col, test_col=test_col,
            evaluation=evaluation)
        test_df, test_feats = self._prepare_data(test_dsets, dset='test',
            algorithms=algorithms, use_image_quality=use_image_quality,
            train_col=train_col, val_col=val_col, test_col=test_col,
            evaluation=evaluation)
        feats = ut.get_common_features(train_feats, test_feats, val_feats)
        model = ut.get_model(model_type, param_search, input_dim=len(feats))
        norm_train_df = self._normalize(train_df, feats, normalization)
        norm_test_df = self._normalize(test_df, feats, normalization)

        if 'val' in data.keys():
            norm_val_df = self._normalize(val_df, feats, normalization)

        # training, testing, evaluating
        frames = (norm_train_df, norm_test_df, norm_val_df, feats)
        scores, importances = self._cross_compare(frames, model,
                m_type=model_type, splits=gs_folds, verbose=verbose,
                k=explain_preds, correct=explain_correct,
                explain_features=explain_features)
        ut.print_scores(scores)
        ut.plot_auroc(scores['roc'], scores['roc_auc'], dset=te_name,
                dir=Global.out_d + 'curves/')

        if len(importances) > 0:
            importances = ut.save_importances(feats, importances,
                    dataset=tr_name, dir=Global.out_d + 'importance_csv/')
            suffix = '_%s_%s' % (model_type, algorithms)
            ut.plot_importances(importances, dataset=tr_name, suffix=suffix,
                    dir=Global.out_d + 'importances/')

    def cross_val(self, data, param_search=None, cv_folds=10, gs_folds=10,
            verbose=2, model_type='xgb', normalization='minmax',
            algorithms='base', use_image_quality=True, train_col='both',
            evaluation='cv'):

        dsets, name = data['train']

        # data preparation
        df, feats = self._prepare_data(dsets, algorithms=algorithms,
            use_image_quality=use_image_quality, train_col=train_col,
            evaluation=evaluation)
        model = ut.get_model(model_type, param_search, input_dim=len(feats))
        norm_df = self._normalize(df, feats, normalization)
        norm_df['fold'] = ut.generate_folds(norm_df)

        # training, testing, evaluating
        scores, importance = self._cross_val(norm_df, feats, model, model_type,
                cv_folds=cv_folds, gs_folds=gs_folds, verbose=verbose)
        ut.print_scores(scores)

    def train_test(self, data, param_search='low', gs_folds=10, verbose=1,
            explain_preds=0, explain_features=False, model_type='xgb',
            normalization='minmax', algorithms='base', use_image_quality=True,
            train_col='both', val_col='both', test_col='both',
            evaluation='tt'):
        train_dsets, tr_name = data['train']
        test_dsets, te_name = data['test']

        val_feats = None
        norm_val_df = None
        if 'val' in data.keys():
            val_dsets, va_name = data['val']
            val_df, val_feats = self._prepare_data(val_dsets, dset='val',
            algorithms=algorithms, use_image_quality=use_image_quality,
            train_col=train_col, val_col=val_col, test_col=test_col,
            evaluation=evaluation)

        # data preparation
        train_df, train_feats = self._prepare_data(train_dsets, dset='train',
            algorithms=algorithms, use_image_quality=use_image_quality,
            train_col=train_col, val_col=val_col, test_col=test_col,
            evaluation=evaluation)
        test_df, test_feats = self._prepare_data(test_dsets, dset='test',
            algorithms=algorithms, use_image_quality=use_image_quality,
            train_col=train_col, val_col=val_col, test_col=test_col,
            evaluation=evaluation)
        feats = ut.get_common_features(train_feats, test_feats, val_feats)
        model = ut.get_model(model_type, param_search, input_dim=len(feats))
        norm_train_df = self._normalize(train_df, feats, normalization)
        norm_test_df = self._normalize(test_df, feats, normalization)

        if 'val' in data.keys():
            norm_val_df = self._normalize(val_df, feats, normalization)

        # training and testing
        frames = (norm_train_df, norm_test_df, norm_val_df, feats)
        self._train_test(frames, model)

    # private
    def _cross_compare(self, data, model, splits=10, m_type='rf', verbose=1,
            k=0, correct=True, explain_features=False):
        train_df, test_df, val_df, feats = data
        x_train, y_train = train_df[feats].values, train_df['label'].values
        x_test, y_test = test_df[feats].values, test_df['label'].values

        val_data = None
        if val_df is not None:
            x_val, y_val = val_df[feats].values, val_df['label'].values
            train_fold = np.full(len(x_train), -1)
            val_fold = np.full(len(x_val), 0)
            predefined_fold = np.append(train_fold, val_fold)
            val_data = (predefined_fold, x_val, y_val)

        mod, params = model
        n_jobs = 1 if m_type == 'ker' else 2
        clf = self._train(mod, params, train_df, x_train, y_train,
                splits=splits, verbose=verbose, val_data=val_data,
                n_jobs=n_jobs)
        y_score = self._test(clf, x_test)
        scores = ut.score(clf, y_score, y_test)
        importances = ut.get_importances(clf, m_type=m_type)

        if m_type == 'xgb':
            self._explain(clf, x_test, y_score, feats, y_true=y_test, k=k,
                    correct=correct, explain_features=explain_features)

        return scores, importances

    def _cross_val(self, data_df, feats, model_info, m_type='rf',
            cv_folds=10, gs_folds=10, verbose=2):
        mod, params = model_info

        for i, fold in enumerate(range(cv_folds)):
            ut.out('fold %d...' % (i + 1))

            train_df = data_df[data_df['fold'] != fold]
            test_df = data_df[data_df['fold'] == fold]

            x_train, y_train = train_df[feats].values, train_df['label'].values
            x_test, y_test = test_df[feats].values, test_df['label'].values

            clf = self._train(mod, params, train_df, x_train, y_train,
                    splits=gs_folds, verbose=verbose)
            y_score = self._test(clf, x_test)
            scores = ut.score(clf, y_score, y_test)
            importances = ut.get_importances(clf, m_type=m_type)

        return scores, importances

    def _explain(self, clf, x_test, y_score, feats, y_true=None, k=0,
            correct=True, explain_features=False):
        est = clf.best_estimator_
        if k > 0:
            self._explain_preds(est, x_test, y_score, feats,
                    y_true=y_true, k=k, correct=correct)
        if explain_features:
            self._explain_features(est, x_test, feats)

    def _explain_features(self, est, x_test, feats):
        ut.out('generating feature contribution plots...')
        out_dir = Global.out_d + 'contributions/'
        exp.feature_contribs(est, x_test, feats, dir=out_dir)

    def _explain_preds(self, est, x_test, y_score, feats, y_true=None,
            targets=['not manipulated', 'manipulated'], k=2, correct=True):
        indices = self._extreme_preds(y_score, y_true=y_true,
                correct=correct, k=k)
        out_dir = Global.out_d + 'explanations/'

        for ndx in indices:
            ut.out('explaining instance %d...' % (ndx))
            x = [x_test[ndx]]  # grab instance to explain
            exp.explain_instance(x, est, feats, targets, fname=str(ndx),
                dir=out_dir)

    def _extreme_preds(self, y_score, y_true=None, correct=False, k=2):
        if y_true is None:
            topk = random.sample(range(len(y_score)), k)
        else:
            mags = np.abs(np.subtract(y_score, y_true))
            mags = [(v, i) for i, v in enumerate(mags)]
            mags_sorted = sorted(mags, key=itemgetter(0), reverse=not correct)
            indices = [i for v, i in mags_sorted]
            topk = indices[:k]
        return topk

    def _fill_in_data(self, df, feats):
        for feat in feats:
            df[feat] = df[feat].replace([np.inf, -np.inf], np.nan)
        df[feats] = df[feats].fillna(df[feats].mean(axis=0))
        return df

    def _normalize(self, df, feats, normalization='minmax'):
        clean_df = self._fill_in_data(df, feats)
        norm_df = d.normalize_df(clean_df, feats, normalization)
        return norm_df

    def _prepare_data(self, dsets, dset='train', evaluation='cc',
            use_image_quality=True, algorithms='base', train_col='both',
            val_col='both', test_col='both'):
        df = d.merge_global_info(dsets, dset=dset, evaluation=evaluation,
                use_image_quality=use_image_quality, algorithms=algorithms,
                train_col=train_col, val_col=val_col, test_col=test_col)
        dropped_df = self._drop_missing_data_from_df(df)
        feats = ut.get_feature_list(dropped_df)
        return df, feats

    def _train(self, clf, params, train_df, x_train, y_train, splits=10,
            verbose=1, n_jobs=2, scoring='roc_auc', val_data=None):
        if val_data is not None:
            val_fold, x_val, y_val = val_data
            x_train_val = np.append(x_train, x_val, axis=0)  # by rows
            y_train_val = np.append(y_train, y_val)
            ps = PredefinedSplit(val_fold)
            cv = ps.split(x_train_val, y_train_val)
        else:
            gkf = GroupKFold(n_splits=splits)
            groups = ut.generate_folds(train_df)
            cv = gkf.split(x_train, y_train, groups=groups)

        n_jobs = 1
        clf = GridSearchCV(clf, params, scoring=scoring, cv=cv,
                verbose=verbose, n_jobs=n_jobs)
        clf.fit(x_train_val, y_train_val)
        ut.out(str(clf.best_params_))
        clf.best_estimator_.fit(x_train, y_train)
        return clf

    def _test(self, clf, x_test):
        preds = clf.predict_proba(x_test)
        y_score = preds[:, 1]
        return y_score

    def _train_test(self, data, model, verbose=1, gs_folds=10,
            explain_preds=0, explain_features=False):
        train_df, test_df, val_df, feats = data
        x_train, y_train = train_df[feats].values, train_df['label'].values
        x_test = test_df[feats].values

        val_data = None
        if val_df is not None:
            x_val, y_val = val_df[feats].values, val_df['label'].values
            train_fold = np.full(len(x_train), -1)
            val_fold = np.full(len(x_val), 0)
            predefined_fold = np.append(train_fold, val_fold)
            val_data = (predefined_fold, x_val, y_val)

        mod, params = model
        clf = self._train(mod, params, train_df, x_train, y_train,
                val_data=val_data, verbose=verbose, splits=gs_folds)
        y_score = self._test(clf, x_test)
        self._explain(clf, x_test, y_score, feats, k=explain_preds,
                explain_features=explain_features)

        # prepare submission
        sub = list(zip(test_df['image_id'], y_score))
        sub_df = pd.DataFrame(sub, columns=['ProbeFileID', 'ConfidenceScore'])
        sub_df['OutputProbeMaskFileName'] = np.nan
        sub_df['IsOptOut'] = 'N'
        sub_df.to_csv('p-all_1_global.csv', sep='|', index=None)


class Local(Task):

    def cross_compare(self, **kwargs):
        ut.out('Cross-compare not implemented for Localization...')
        exit(0)

    def cross_val(self, **kwargs):
        ut.out('Cross-val not implemented for Localization...')
        exit(0)

    def train_test(self, data, param_search=None, gs_folds=2, verbose=1,
            explain_preds=0, explain_correct=True, explain_features=False,
            model_type='xgb', normalization='minmax', algorithms='base',
            use_image_quality=True, train_col='both', val_col='both',
            test_col='both'):

        train_dsets, tr_name = data['train']
        test_dsets, te_name = data['test']

        start = t.time()
        train_dfs = d.merge_local_info(train_dsets, dset='train',
                algorithms=algorithms)
        test_dfs = d.merge_local_info(test_dsets, dset='test',
                algorithms=algorithms)
        ut.out('merging data...%.2fm' % ((t.time() - start) / 60.0))

        start = t.time()
        train_df = self._dfs_to_df(train_dfs)
        test_df = self._dfs_to_df(test_dfs)
        drop_train_df = self._drop_missing_data_from_df(train_df)
        drop_test_df = self._drop_missing_data_from_df(test_df)
        ut.out('dropping empty data...%.2fm' % ((t.time() - start) / 60.0))

        start = t.time()
        train_feats = ut.get_feature_list(drop_train_df)
        test_feats = ut.get_feature_list(drop_test_df)
        feats = ut.get_common_features(train_feats, test_feats)
        model = ut.get_model(model_type, param_search, input_dim=len(feats))
        ut.out('finding common features...%.2fm' % ((t.time() - start) / 60.0))

        start = t.time()
        clean_train_dfs, means = self._fill_in_data(train_dfs, feats)
        clean_test_dfs, _ = self._fill_in_data(test_dfs, feats, means)
        ut.out('filling in missing data...%.2fm' % ((t.time() - start) / 60.0))

        start = t.time()
        norm_train_dfs = d.normalize_dfs(clean_train_dfs, feats, normalization)
        norm_test_dfs = d.normalize_dfs(clean_test_dfs, feats, normalization)
        ut.out('normalizing data...%.2fm' % ((t.time() - start) / 60.0))

        frames = (norm_train_dfs, tr_name, norm_test_dfs, te_name, feats)
        self._train_test(frames, model, m_type=model_type, gs_folds=gs_folds)

    # private
    def _dfs_to_df(self, dfs):
        df_list = []

        for df, mask, size_sm, size_og in dfs:
            df_list.append(df)
        df = pd.concat(df_list)
        df = df.reset_index().drop(['index'], axis=1)
        return df

    def _drop_missing_data(self, dfs):
        result = []

        for df, mask, size_sm, size_og in dfs:
            df = df.dropna(axis=1, how='all')
            result.append((df, mask, size_sm, size_og))
        return result

    def _fill_in_data(self, dfs, feats, feat_means=None):
        result = []

        # obtain feature means if there are none
        if feat_means is None:
            feat_means = {}
            big_df = pd.DataFrame()
            temp = []

            for df, mask, size_sm, size_og in dfs:
                temp.append(df)
            big_df = pd.concat(temp)
            big_df = big_df.reset_index().drop(['index'], axis=1)

            for feat in feats:
                feat_means[feat] = big_df[feat].mean()

        # fill in missing values with the means for that feature
        for df, mask, size_sm, size_og in dfs:
            for feat in feats:
                df[feat] = df[feat].fillna(feat_means[feat])
            result.append((df, mask, size_sm, size_og))

        return result, feat_means

    def _test(self, clf, test_dfs, feats, out_d=''):
        out_dir = 'output/local/' + out_d + '/masks/'
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        i, rows = 0, []
        for df, mask, size_sm, size_og in test_dfs:
            i += 1
            if i % 500 == 0:
                ut.out('(%d/%d)' % (i, len(test_dfs)))

            # predict pixels for this image
            x_test = df[feats].values
            preds = clf.predict_proba(x_test)
            y_score = preds[:, 1]

            # turn predictions into image
            width_sm, height_sm = size_sm['width'], size_sm['height']
            pixels_inverted = d.scale_data(y_score)
            pixels = [255 - p for p in pixels_inverted]
            img_data = np.asarray(pixels).reshape((height_sm, width_sm))
            fname = out_dir + mask
            skio.imsave(fname, img_data)

            # resize image to original size
            size_og = size_og['width'], size_og['height']
            img = Image.open(fname)
            img = img.resize(size_og, Image.ANTIALIAS)
            img_data = np.asarray(img, dtype='int32')
            skio.imsave(fname, img_data)

            # build system csv file
            probe_file_id = mask.replace('.png', '')
            rows.append((probe_file_id, 'masks/' + mask, 1.0))

        cols = ['ProbeFileID', 'OutputProbeMaskFileName', 'ConfidenceScore']
        sys_df = pd.DataFrame(rows, columns=cols)
        sys_file = 'output/local/' + out_d + '/sys.csv'
        sys_df.to_csv(sys_file, sep='|', index=None)

    def _train(self, clf, params, train_df, x_train, y_train, gs_folds=10,
            verbose=1, n_jobs=1, scoring='roc_auc', val_fold=None):

        # TODO: fix val_fold training
        if val_fold is not None:
            ps = PredefinedSplit(val_fold)
            cv = ps.split(x_train, y_train)
        else:
            gkf = GroupKFold(n_splits=gs_folds)
            groups = ut.generate_folds(train_df)
            cv = gkf.split(x_train, y_train, groups=groups)

        print(train_df)
        clf = GridSearchCV(clf, params, scoring=scoring, cv=cv,
                verbose=verbose, n_jobs=n_jobs)
        clf.fit(x_train, y_train)
        ut.out(str(clf.best_params_))
        return clf

    def _train_test(self, data, model, gs_folds=10, m_type='rf'):
        train_dfs, train_name, test_dfs, test_name, feats = data
        train_df = self._dfs_to_df(train_dfs)

        x_train, y_train = train_df[feats].values, train_df['label'].values
        mod, params = model

        start = t.time()
        clf = self._train(mod, params, train_df, x_train, y_train, gs_folds)
        ut.out('training...%.2f' % ((t.time() - start) / 60.0))

        start = t.time()
        self._test(clf, test_dfs, feats, out_d=train_name + '+' + test_name)
        ut.out('testing...%.2f' % ((t.time() - start) / 60.0))

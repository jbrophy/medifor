import os
import numpy as np
import pandas as pd
import skimage.io as skio
from ..utility import util as ut


# public
def get_data_dict():
    """
    Dictionary containing datasets: {'1': 2016, '2': 2017: '3': 2017_Eval,
    '4': 2018}.

    Returns
    -------
    Dictionary of of valid datasets to use for reasoning.
    """
    data_dict = {'1': 'UOML_NC2016_Test0613',
            '2': 'NC2017_Dev_Ver1', '3': 'NC17_Eval',
            '4': 'MFC18_Dev_Image_Ver1'}
    return data_dict


def get_data(train_keys=None, val_keys=None, test_keys=None):
    """
    Retrieves a set of dataset names based on the given keys.

    Parameters
    ----------
    train_keys : str, default: None
        Valid data key combination (e.g. '1', '12', '123', etc.). See
        `get_data_dict()` to see which keys correspond to which datasets.
    val_keys : str, default: None
        See `train_keys`.
    test_keys : str, default: None
        see `train_keys`.

    Returns
    -------
    Dictionary containing 'train', 'val', and 'test' keys where the values
    are a list of dataset names and one name describing them all.
    """
    data = {}
    data_dict = get_data_dict()

    if train_keys is not None:
        train_datasets = [data_dict[key] for key in train_keys]
        train_name = '+'.join(train_datasets)
        data['train'] = (train_datasets, train_name)

    if val_keys is not None:
        val_datasets = [data_dict[key] for key in val_keys]
        val_name = '+'.join(val_datasets)
        data['val'] = (val_datasets, val_name)

    if test_keys is not None:
        test_datasets = [data_dict[key] for key in test_keys]
        test_name = '+'.join(test_datasets)
        data['test'] = (test_datasets, test_name)

    return data


def merge_global_info(datasets, dset='train', evaluation='cc',
        use_image_quality=True, algorithms='base', train_col='both',
        test_col='both', val_col='both'):
    """
    Fetches the images in `datasets` and merges on: indicator data, labels,
    and journal information.

    Parameters
    ----------
    datasets : list
        List of dataset names to retrieve data for.
    dset : str, 'train', 'val' or 'test', default: 'train'
        Describes which dataset this is.
    see `app.app` for a description of the other parameters.

    Returns
    -------
    Fully merged dataframe containing all the data from `datasets`.
    """
    full_df = pd.DataFrame()
    datasets = [d + '_40' for d in datasets]

    for dataset in datasets:

        # merge image_ids and/or labels
        if evaluation == 'tt' and dset == 'test':
            index_fname = 'data/' + dataset + '/indexes/index.csv'
            df = pd.read_csv(index_fname, sep='|')
            df = df[['ProbeFileID']]
            df.columns = ['image_id']
        else:
            targets_folder = 'data/' + dataset + '/targets/manipulation/'
            df = pd.read_csv(targets_folder + 'reference.csv', sep='|')
            df.columns = ['image_id', 'label']
            df['label'] = df['label'].apply(lambda x: 0 if x == 255 else 1)

        # merge reference information & split 2016 into sci and web
        if '2016' in dataset:
            ref_folder = 'data/' + dataset + '/reference/manipulation/'
            fname = ref_folder + 'NC2016-manipulation-ref.csv'
            temp = pd.read_csv(fname, sep='|')
            temp = temp[['ProbeFileID', 'Collection']]
            temp.columns = ['image_id', 'collection']

            if dset == 'train' and train_col != 'both':
                temp = temp[temp['collection'] == train_col]
            elif dset == 'test' and test_col != 'both':
                temp = temp[temp['collection'] == test_col]
            elif dset == 'val' and val_col != 'both':
                temp = temp[temp['collection'] == val_col]

            df = temp.merge(df, on='image_id', how='left')
            df = df.drop(['collection'], axis=1)

        # merge journal information
        if evaluation == 'cc' and '17' in dataset:
            ref_folder = 'data/' + dataset + '/reference/manipulation/'
            fname = ref_folder + 'image-ref.csv'
            temp = pd.read_csv(fname, sep='|')
            temp = temp[['ProbeFileID', 'JournalName']]
            temp.columns = ['image_id', 'journal']
            df = df.merge(temp, on='image_id', how='left')

        # merge image quality information
        if use_image_quality:
            fname = 'data/' + dataset + '/reference/image_quality.csv'
            if os.path.exists(fname):
                temp = pd.read_csv(fname)
                df = df.merge(temp, on='image_id', how='left')

        # merge indicators
        indicators_folder = 'data/' + dataset + '/indicators/'
        all_files = os.listdir(indicators_folder)
        indicators = [f for f in all_files if not f.startswith('.')]

        if algorithms == 'base':
            indicators = [i for i in indicators if 'baseline' in i]

        for indicator in indicators:
            fname = indicators_folder + indicator + '/reference.csv'
            temp_df = pd.read_csv(fname, sep='|')
            temp_df.columns = ['image_id', indicator]
            df = df.merge(temp_df, on='image_id', how='left')

        full_df = full_df.append(df)

    full_df = full_df.reset_index()
    full_df = full_df.drop(['index'], axis=1)
    return full_df


def merge_local_info(datasets, dset='train', algorithms='base'):
    """
    Fetches the images in `datasets`, finds their dimensions and merges on
    indicator data and labels.

    Parameters
    ----------
    datasets : list
        List of dataset names to retrieve data for.
    dset : str, 'train', 'val' or 'test', default: 'train'
        Describes which dataset this is.
    algorithms : str, 'base' or 'all', default: 'base'
        Whether or not to use baseline only or all algorithms.

    Returns
    -------
    List of merged dataframes containing all the data from `datasets` for
    each image.
    """
    dfs = []

    for dataset in datasets:

        # merge image_ids and dimensions
        index_file = 'data/' + dataset + '/indexes/index.csv'
        if not os.path.exists(index_file):
            mask_d = 'data/' + dataset + '/targets/manipulation/mask/'
            masks = os.listdir(mask_d)
            masks = [m for m in masks if not m.startswith('.')]

            rows = []
            for mask in masks:
                command = 'identify -ping -format "%w,%h" ' + mask_d + mask
                size_str = os.popen(command).read()
                size = size_str.split(',')
                image_id = mask.replace('.png', '')
                rows.append((image_id, int(size[0]), int(size[1])))
            df = pd.DataFrame(rows, columns=['image_id', 'width', 'height'])
        else:
            df = pd.read_csv(index_file, sep='|')
            df = df[['ProbeFileID', 'ProbeWidth', 'ProbeHeight']]
            df.columns = ['image_id', 'width', 'height']

        # get list of indicators
        indicators_folder = 'data/' + dataset + '_40/indicators/'
        all_files = os.listdir(indicators_folder)
        indicators = [f for f in all_files if not f.startswith('.')]

        if algorithms == 'base':
            indicators = [i for i in indicators if 'baseline' in i]

        # build separate dataframes of pixel values for each image
        ut.out('dset: %s' % dataset)
        j = 0
        for row in df.itertuples():
            j += 1
            if j % 500 == 0:
                ut.out('(%d/%d)' % (j, len(df)))

            temp = pd.DataFrame()
            image_id, width_og, height_og = row[1], row[2], row[3]
            size_og = {'height': height_og, 'width': width_og}
            mask = image_id + '.png'

            first = False
            valid = True
            size_sm = None

            for i, indicator in enumerate(indicators):
                ind_mask_f = indicators_folder + indicator + '/mask/'
                path = ind_mask_f + mask
                if os.path.exists(path) and os.stat(path).st_size > 0:
                    d = skio.imread(path)
                    if not first:
                        first = True
                        size_sm = {'height': len(d), 'width': len(d[0])}
                        temp[indicator] = d.flatten()
                    else:
                        area = size_sm['height'] * size_sm['width']
                        if len(d.flatten()) == area:
                            temp[indicator] = d.flatten()
                        else:
                            temp[indicator] = np.nan
                else:
                    temp[indicator] = np.nan

                # add labels to training dataframe
                if dset == 'train':
                    data_f = 'data/' + dataset + '_40'
                    target_f = data_f + '/targets/manipulation/mask/'

                    if os.path.exists(target_f + mask):
                        d = skio.imread(target_f + mask)
                        temp['label'] = d.flatten()
                        labeling = lambda x: 0 if x == 255 else 1
                        temp['label'] = temp['label'].apply(labeling)
                    else:
                        valid = False

            temp = temp.reset_index().drop(['index'], axis=1)
            if len(temp) > 0 and valid:
                dfs.append((temp, mask, size_sm, size_og))
    ut.out(str(len(dfs)))
    return dfs


def normalize_df(df, feats, normalization='minmax'):
    """
    Normalize a single dataframe.

    Parameters
    ----------
    df : pd.DataFrame
        Pandas dataframe.
    feats : list
        List of features to normalize.
    normalization : str, 'minmax' or 'std_dev', default: 'minmax'
        Normalization stratgey to use.

    Returns
    -------
    Normalized dataframe.
    """
    result = df.copy()
    new_min, new_max = 0.0, 1.0

    stat_dict = _get_feature_dict(normalization)

    for feat in feats:
        if len(df.groupby(feat).size().reset_index()) > 1:
            if normalization == 'minmax':
                old_min, old_max = stat_dict[feat]
                result[feat] = scale_data(result[feat], old_min,
                    old_max, new_min, new_max, round_result=False)
            else:
                result[feat] = ut.div0(result[feat], stat_dict[feat])
    return result


def normalize_dfs(dfs, feats, normalization='minmax'):
    """
    Normalize a list of dataframes.

    Parameters
    ----------
    dfs : list
        List of dataframes to normalize.
    feats : list
        List of features to normalize in each dataframe.
    normalization : str, 'minmax' or 'std_dev', default: 'minmax'.
        Strategy to normalize values with.

    Returns
    -------
    List of normalized dataframes.
    """
    result = []

    for df, mask, size_sm, size_og in dfs:
        norm_df = normalize_df(df, feats, normalization)
        result.append((norm_df, mask, size_sm, size_og))
    return result


def scale_data(vals, old_min=0.0, old_max=1.0, new_min=0.0,
        new_max=255.0, round_result=True):
    scale = ut.div0((new_max - new_min), (old_max - old_min))
    result = [scale * (v - old_max) + new_max for v in vals]
    if round_result:
        result = [int(round(r)) for r in result]
    return result


def setup_local_mask_scoring(dsets, out_d=''):
    """
    Sets up the directory with the appropriate files for Mediscore.

    Parameters
    ----------
    dsets : list
        List of dataset names corresponding to the local model output.
    out_d : str, default=''
        Directory to save files to.
    """
    index_cols = ['TaskID', 'ProbeFileID', 'ProbeFileName', 'ProbeWidth',
            'ProbeHeight']
    ref_cols = ['TaskID', 'ProbeFileID', 'ProbeFileName',
            'IsTarget', 'ProbeMaskFileName', 'BaseFileName']
    ref_pjj_cols = ['ProbeFileID', 'JournalName', 'StartNodeID', 'EndNodeID',
            'Sequence']
    ref_jm_cols = ['JournalName', 'StartNodeID', 'EndNodeID', 'Operation',
            'Color', 'Purpose', 'OperationArgument']
    index_df = pd.DataFrame()
    ref_df = pd.DataFrame()
    pjj_df = pd.DataFrame()
    jm_df = pd.DataFrame()

    for dset in dsets:
        index_d = 'data/' + dset + '/indexes/'
        ref_d = 'data/' + dset + '/reference/manipulation/'
        target_d = '../../../data/' + dset + '/targets/manipulation/mask/'
        probe_d = '../../../data/' + dset + '/'

        pfn_col = 'ProbeFileName'
        pmfn_col = 'ProbeMaskFileName'
        temp_ref_df = pd.read_csv(ref_d + 'image-ref.csv', sep='|')
        temp_ref_df[pmfn_col] = target_d + temp_ref_df['ProbeFileID'] + '.png'
        temp_ref_df[pfn_col] = probe_d + temp_ref_df['ProbeFileID']
        temp_ref_df = temp_ref_df[ref_cols]

        if os.path.exists(index_d + 'index.csv'):
            temp_index_df = pd.read_csv(index_d + 'index.csv', sep='|')
        else:
            vals = [('e', 'e', 'e', 'e', 'e')]
            temp_index_df = pd.DataFrame(vals, columns=index_cols)

        if os.path.exists(ref_d + 'probejournaljoin.csv'):
            temp_pjj_df = pd.read_csv(ref_d + 'probejournaljoin.csv', sep='|')
        else:
            vals = [('e', 'e', 'e', 'e', 'e')]
            temp_pjj_df = pd.DataFrame(vals, columns=ref_pjj_cols)

        if os.path.exists(ref_d + 'journalmask.csv'):
            temp_jm_df = pd.read_csv(ref_d + 'journalmask.csv', sep='|')
        else:
            vals = [('e', 'e', 'e', 'e', '0 0 0', 'e', 'e')]
            temp_jm_df = pd.DataFrame(vals, columns=ref_jm_cols)
        temp_jm_df['Color'] = '0 0 0'

        index_df = index_df.append(temp_index_df)
        ref_df = ref_df.append(temp_ref_df)
        pjj_df = pjj_df.append(temp_pjj_df)
        jm_df = jm_df.append(temp_jm_df)

    index_df.to_csv(out_d + 'index.csv', sep='|', index=None)
    ref_df.to_csv(out_d + 'ref.csv', sep='|', index=None)
    pjj_df.to_csv(out_d + 'ref-probejournaljoin.csv', sep='|', index=None)
    jm_df.to_csv(out_d + 'ref-journalmask.csv', sep='|', index=None)


# private
def _get_feature_dict(normalization='minmax'):
    d = {}
    df = pd.read_csv('statistics/stats.csv')

    if normalization == 'minmax':
        stats = list(zip(df['min'], df['max']))
    else:
        stats = list(df['std'])

    for i, feat in enumerate(df['alg']):
        d[feat] = stats[i]
    return d
import os
import sys
import numpy as np
import pandas as pd
import xgboost as xgb
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import log_loss
from keras.layers import Dense
from keras.models import Sequential
from keras.wrappers.scikit_learn import KerasClassifier


def div0(num, denom):
    """
    Safe divide that handles division by zero.

    Parameters
    ----------
    num : int or float
        Numerater in division.
    denom : int or float
        Denominator in division.

    Returns
    -------
    Division of numerator and denominator as a float.
    """
    if denom == 0:
        result = 0
    else:
        result = num / float(denom)
    return result


def generate_folds(df, num_folds=10, seed=69):
    """
    Partitions rows in a dataframe into different groups.

    Parameters
    ----------
    df : pd.DataFrame
        Dataframe containing rows of data.
    num_folds : int, default: 10
        Number of groups to seperate rows into.
    seed : int, default: 69
        Random seed generator for reproducibility.

    Returns
    -------
    List of group numbers corresponding to the group each row belongs to.
    """
    temp = df.copy()

    if 'journal' not in list(df):
        np.random.seed(seed)
        temp['temp'] = np.random.randint(num_folds, size=len(temp))
    else:
        np.random.seed(seed)
        other = temp[pd.isnull(temp['journal'])]
        g = other.groupby('image_id').size().reset_index()
        g['group_other'] = np.random.randint(num_folds, size=len(g))
        g = g[['image_id', 'group_other']]
        temp = temp.merge(g, on='image_id', how='left')

        np.random.seed(seed)
        g = temp.groupby('journal').size().reset_index()
        g['group_temp'] = np.random.randint(num_folds, size=len(g))
        g = g[['journal', 'group_temp']]
        temp = temp.merge(g, on='journal', how='left')

        temp['temp'] = temp['group_other'].fillna(temp['group_temp'])
        temp = temp.drop(['group_other', 'group_temp', 'journal'], axis=1)

    temp['temp'] = temp['temp'].apply(int)
    return temp['temp']


def get_common_features(train_feats, test_feats, val_feats=None):
    """
    Find features common to all datasets.

    Parameters
    ----------
    train_feats : list
        List of feature names in the training data.
    test_feats : list
        List of feature names in the testing data.
    val_feats : list, default: None
        List of feature names in the validation data.

    Returns
    -------
    List of features common to all the datasets.
    """
    feats = [f for f in train_feats if f in test_feats]
    if val_feats is not None:
        feats = [f for f in feats if f in val_feats]
    out(str((feats, len(feats))))
    return feats


def get_feature_list(df, exclude=['image_id', 'label', 'journal']):
    """
    Retrieves features within a dataframe.

    Parameters
    ----------
    df : pandas.DataFrame
        Dataframe to extract features from.
    exclude : list, default: ['image_id', 'label', 'journal']
        List of strings to not include in list of features.

    Returns
    -------
    List of features as strings.
    """
    features = list(df)
    for item in exclude:
        if item in features:
            features.remove(item)
    return features


def get_importances(clf, m_type='rf'):
    estimator = clf.best_estimator_
    importance_scores = []

    if m_type in ['rf', 'xgb']:
        importance_scores.append(estimator.feature_importances_)
    elif m_type in ['lr1', 'lr2']:
        importance_scores.append(estimator.coef_[0])
    return importance_scores


def get_model(model_type='rf', param_search='low', input_dim=0):
    if model_type == 'lr1':
        clf = LogisticRegression()
        high = [{'penalty': ['l1'],
            'C': [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1.0,
                2.0, 10.0, 50.0, 100.0, 500.0, 1000.0],
            'solver': ['liblinear']}]
        low = med = high

    elif model_type == 'lr2':
        clf = LogisticRegression()
        high = [{'penalty': ['l2'],
            'C': [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1.0,
                2.0, 10.0, 50.0, 100.0, 500.0, 1000.0],
            'solver': ['liblinear', 'newton-cg']}]
        med = [{'penalty': ['l2'],
            'C': [0.01, 1.0, 100.0],
            'solver': ['liblinear', 'newton-cg']}]
        low = high

    elif model_type == 'rf':
        clf = RandomForestClassifier()
        high = {'n_estimators': [10, 100, 1000],
            'max_depth': [None, 2, 4]}
        med = {'n_estimators': [1000],
            'max_depth': [None, 2]}
        low = {'n_estimators': [1000],
            'max_depth': [None]}

    elif model_type == 'xgb':
        clf = xgb.XGBClassifier()
        high = {'max_depth': [3, 4, 6],
                'n_estimators': [100, 1000],
                'learning_rate': [0.3, 0.1, 0.05, 0.01, 0.005, 0.001],
                'subsample': [0.8, 0.9, 1.0],
                'colsample_bytree': [0.8, 0.9, 1.0]}
        med = {'max_depth': [4, 6], 'n_estimators': [1000],
               'learning_rate': [0.005, 0.05, 0.1], 'subsample': [0.9, 1.0],
               'colsample_bytree': [1.0]}
        low = {'max_depth': [6], 'n_estimators': [1000],
               'learning_rate': [0.005], 'subsample': [0.9],
               'colsample_bytree': [1.0]}

    elif model_type == 'ker':
        print(input_dim)
        clf = KerasClassifier(build_fn=_keras_model(input_dim=input_dim))
        high = {'optimizer': ['adam', 'sgd', 'rmsprop'],
                'loss': ['binary_crossentropy', 'mean_squared_error'],
                'epochs': [50, 100, 150], 'batch_size': [50, 100, 150]}
        med = {'optimizer': ['sgd', 'rmsprop'],
               'loss': ['binary_crossentropy'],
               'epochs': [50, 150], 'batch_size': [50, 150]}
        low = {'optimizer': ['rmsprop'], 'loss': ['binary_crossentropy'],
               'epochs': [150], 'batch_size': [100]}

    param_dict = {'high': high, 'med': med, 'low': low}
    param_grid = param_dict[param_search]
    return (clf, param_grid)


def out(message=''):
    sys.stdout.write(message + '\n')
    sys.stdout.flush()


def plot_auroc(roc, auc, title='', dir='output/curves/', line='-', dset=''):
    if not os.path.exists(dir):
        os.makedirs(dir)

    fpr, tpr, thresh = roc

    fig, ax = plt.subplots()
    ax.plot(fpr, tpr, line, label='AUROC' + ' = %0.3f' % auc)
    ax.plot((0.0, 1.0), (0.0, 1.0), 'k--')
    ax.set_xlim([0.0, 1.0])
    ax.set_ylim([0.0, 1.0])
    ax.set_title(title, fontsize=22)
    ax.set_xlabel('False positive rate', fontsize=22)
    ax.set_ylabel('True positive rate', fontsize=22)
    ax.tick_params(axis='both', labelsize=18)
    ax.legend(loc='lower right', prop={'size': 10})
    ax.set_yticks(np.arange(0.0, 1.01, 0.1))
    ax.set_xticks(np.arange(0.0, 1.01, 0.1))
    ax.set_xticklabels(np.arange(0.0, 1.01, 0.1), rotation=70)
    plt.savefig(dir + dset + '.pdf', format='pdf', bbox_inches='tight')
    plt.clf()


def plot_importances(feature_scores, dir='output/importances/', dataset='',
        suffix=''):
    if not os.path.exists(dir):
        os.makedirs(dir)

    figname = dir + dataset + suffix + '.pdf'
    plt.rc('pdf', fonttype=42)

    names = [name for (name, score) in feature_scores]
    scores = [score for (name, score) in feature_scores]
    bars = np.arange(len(names))

    f, ax1 = plt.subplots(figsize=(4, 8))
    ax1.barh(bars, scores, 0.3, tick_label=names)
    ax1.set_xlabel('Coefficients')
    ax1.set_title(dataset)
    plt.savefig(figname, format='pdf', bbox_inches='tight')
    plt.clf()


def print_scores(scores):
    metrics = [('AUROC', 'roc_auc'), ('Log Loss', 'log_loss')]

    for metric, key in metrics:
        vals = scores.get(key)
        if vals is not None:
            vals = [vals] if not isinstance(vals, list) else vals
            mean = np.mean(vals)
            std = np.std(vals)
            out('%s: %.4f +/- %.4f' % (metric, mean, std))


def save_importances(indicators, importances, dir='output/importance_csv/',
        dataset=''):
    if not os.path.exists(dir):
        os.makedirs(dir)

    mean_scores = np.mean(importances, axis=0)
    importance_scores = list(zip(indicators, mean_scores))
    temp = pd.DataFrame(importance_scores, columns=['alg', 'importance'])
    temp.to_csv(dir + dataset + '.csv', index=None)
    return importance_scores


def score(self, y_score, y_true, metrics=['roc_auc', 'log_loss']):
    for metric in metrics:
        assert metric in ['roc_auc', 'log_loss']

    scores = {}
    scores['roc'] = roc_curve(y_true, y_score)
    scores['roc_auc'] = roc_auc_score(y_true, y_score)
    scores['log_loss'] = log_loss(y_true, y_score)
    return scores


# private
def _keras_model(loss='binary_crossentropy', optimizer='adam',
        metrics=['accuracy'], activation='sigmoid', input_dim=None):
    model = Sequential()
    model.add(Dense(1, input_dim=input_dim, activation=activation))
    model.compile(loss=loss, optimizer=optimizer, metrics=metrics)
    return model

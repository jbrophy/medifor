import os
import sys
import time
import pandas as pd
from .model import data as d
from .model import task as t
# from g.model import Dual


def configure_settings(max_columns=100):
    """
    Sets application settings, mainly pertaining to pandas viewing.

    Parameters
    ----------
    max_columns : int, default: 100
        Maximum number of columns to display when viewing a pandas dataframe.
    """
    pd.options.mode.chained_assignment = None
    if os.isatty(sys.stdin.fileno()):
        rows, columns = os.popen('stty size', 'r').read().split()
        pd.set_option('display.width', int(columns))
        pd.set_option('display.max_columns', max_columns)


def run(task='global', model_type='xgb', use_image_quality=True,
        use_dual=False, evaluation='cc', train_sets='1',
        train_col='both', val_sets=None, val_col='both', test_sets='1',
        test_col='both', explain_preds=0, explain_correct=True,
        explain_features=False, algorithms='base', normalization='minmax',
        param_search='low', cv_folds=10, gs_folds=10, verbose=1):
    """
    Runs the reasoning engine of the medifor project.

    Parameters
    ----------
    task : str, 'global' or 'local', default: 'global'
        Manipulation task where 'global' refers to 'detection' and 'local'
        refers to 'localization'.
    model_type : str, 'xgb', 'rf', 'lr1', lr2', or 'ker' default: 'xgb'
        Algorithm to use for classification. 'xgb': xgboost, 'rf': random
        forest, 'lr1' is logistic regression with an l1 penalty, 'lr2' uses
        an l2 penalty, and 'key' is a model built using keras.
    use_image_quality : bool, default: True
        Extra feature to possibly help determine image manipulation.
    use_dual : bool, default: False
        Creates two models: one trained on low quality images, and another
        trained on high quality images. Only used if use_image_quality is
        True.
    evaluation : str, 'cc', 'cv', or 'tt', default: 'cc'
        Train and test procedure. 'cc' cross-comparison trains on one dataset,
        then tests and evaluates on another. 'cv' does cross-validation on a
        single dataset. 'tt' trains on one dataset, and produces predictions
        for another dataset but does not do any evaluation.
    train_sets : str, default: '1'
        Any valid dataset key: '1': 2016, '2': 2017, '3': 2017_Eval,
        '4': 2018. Datasets can also be combined by specifying multiple keys.
        For example: '124' would be 2016+2017+2018 combined.
    train_col : str, 'Nimble-SCI', 'Nimble-WEB', 'both', default: 'both'
        Collection to use. Only valid if `train_sets` contains 2016 as one of
        its datasets.
    val_sets : str, default: None
        See `train_sets`. Only relevant if `evaluation` is 'cc' or 'tt'. Uses
        the validation set to tune hyperparameters. If None, cross-validation
        on the training set is used to tune hyper-parameters.
    val_col : str, 'Nimble-SCI', 'Nimble-WEB', 'both', default: 'both'
        See `train_collection`.
    test_sets : str, default: '1'
        See `train_sets`.
    test_col : str, 'Nimble-SCI', 'Nimble-WEB', 'both', default: 'both'
        See `train_collection`.
    explain_preds : int, default: 0
        Number of instances to explain in the test set. Currently only
        supports a 'cc' `evaluation`.
    explain_correct : bool, default: True
        If True, explains predictions that have the lowest errors. Otherwise,
        explains instances with the highest errors. Only valid if
        `explain_preds` > 0.
    explain_features : bool, default: False
        If True, plots the log odds contributions of feature as a function of
        that features' value in the test set. Only supports 'xgb' `model_type`.
    algorithms : str, 'base', 'all', default: 'base'
        Whether or not to use only baseline or all algorithms in the
        reasoning engine.
    normalization : str, 'minmax' or 'std_dev', default: 'minmax'
        Normalization stratagey to use on the algorithm outputs. 'minmax'
        normalizes based on the min and max values for each algorithm.
        'std_dev' divides each value by it's feature's standard deviation.
    param_search : str, 'low', 'med', 'high', default: 'low'
        Amount of parameters to search over while doing hyper-parameter
        tuning.
    cv_folds : int, 1-10, default: 10
        Number of folds to do while doing cross-validation. Only relevant
        when evaluation is set to 'cv'.
    gs_folds : int, 1-10, default: 10
        Number of folds to do while doing a gridsearch during hyper-paramter
        tuning.
    verbose : int, 1-10, default: 1
        Amount of feedback to print to the screen.
    """

    train_keys = [k for k in train_sets]
    val_keys = [k for k in val_sets] if val_sets is not None else None
    test_keys = [k for k in test_sets]
    data_dict = d.get_data_dict()

    # validate args
    assert task in ['global', 'local']
    assert model_type in ['xgb', 'rf', 'lr1', 'lr2', 'ker']
    assert isinstance(use_image_quality, bool)
    assert isinstance(use_dual, bool)
    assert isinstance(explain_correct, bool)
    assert isinstance(explain_features, bool)
    assert explain_preds >= 0
    assert evaluation in ['cv', 'cc', 'tt']
    assert algorithms in ['base', 'all']
    assert normalization in ['minmax', 'std_dev']
    assert param_search in ['high', 'med', 'low']
    assert cv_folds > 1 and cv_folds <= 10
    assert gs_folds > 1 and gs_folds <= 10
    for key in train_keys:
        assert key in data_dict.keys()
    for key in test_keys:
        assert key in data_dict.keys()
    if val_keys is not None:
        for key in val_keys:
            assert key in data_dict.keys()
    if '1' in train_keys:
        assert train_col in ['Nimble-SCI', 'Nimble-WEB', 'both']
    if '1' in test_keys:
        assert test_col in ['Nimble-SCI', 'Nimble-WEB', 'both']
    if val_keys is not None:
        if '1' in val_keys:
            assert val_col in ['Nimble-SCI', 'Nimble-WEB', 'both']
    if explain_preds or explain_features:
        assert model_type == 'xgb'

    configure_settings()

    # obtain the data
    data = d.get_data(train_keys, val_keys, test_keys)

    # create the right model based on the task
    model = t.Global() if task == 'global' else t.Local()

    # exceute action based on evaluation scheme
    if evaluation == 'cc':
        model.cross_compare(data, param_search=param_search, gs_folds=gs_folds,
            explain_preds=explain_preds, explain_correct=explain_correct,
            explain_features=explain_features, model_type=model_type,
            normalization=normalization, algorithms=algorithms,
            train_col=train_col, val_col=val_col, test_col=test_col,
            use_image_quality=use_image_quality, verbose=verbose)
    elif evaluation == 'cv':
        model.cross_val(data, param_search=param_search, cv_folds=cv_folds,
            gs_folds=gs_folds, verbose=verbose, model_type=model_type,
            normalization=normalization, algorithms=algorithms,
            train_col=train_col, use_image_quality=use_image_quality)
    else:
        model.train_test(data, param_search=param_search, gs_folds=gs_folds,
            model_type=model_type, normalization=normalization,
            algorithms=algorithms, use_image_quality=use_image_quality,
            train_col=train_col, val_col=val_col, test_col=test_col)


def score_local(train_sets=None, test_sets=None):
    """
    Scores the local model masks using Mediscore mask scoring.

    Parameters
    ----------
    train_sets : str, default: '1'
        Any valid dataset key: '1': 2016, '2': 2017, '3': 2017_Eval,
        '4': 2018. Datasets can also be combined by specifying multiple keys.
        For example: '124' would be 2016+2017+2018 combined.
    test_sets : str, default: '1'
        See `train_sets`.
    """
    train_keys = [k for k in train_sets]
    test_keys = [k for k in test_sets]
    for key in train_keys:
        assert key in d.get_data_dict().keys()
    for key in test_keys:
        assert key in d.get_data_dict().keys()

    data = d.get_data(train_keys=train_keys, test_keys=test_keys)
    _, tr_name = data['train']
    dsets, te_name = data['test']
    name = tr_name + '+' + te_name
    name = name.replace('_40', '')
    dsets = [x.replace('_40', '') for x in dsets]

    sys_d = ref_d = 'output/local/' + name + '/'
    assert os.path.exists(ref_d)
    assert os.path.exists(ref_d + 'sys.csv')
    assert os.path.exists(ref_d + 'masks/')
    out_root = sys_d + 'mask_scores/'

    start = time.time()
    d.setup_local_mask_scoring(dsets, ref_d)
    command = 'python3 MediScore/tools/MaskScorer/MaskScorer.py '
    command += '--refDir %s -r ref.csv -x index.csv '
    command += '--sysDir %s -s sys.csv --outRoot %s -html -v 1 --speedup'
    os.system(command % (ref_d, sys_d, out_root))
    print('scoring: %dm' % ((time.time() - start) / 60))

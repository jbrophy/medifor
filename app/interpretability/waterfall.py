import math
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


# public
def waterfall(logits, labels, pos_color='c', neg_color='r',
        total_color='k', ref_line_color='k', bar_edge_color='k',
        bar_line_width=1.0, pred_val='Prediction', fname='waterfall.pdf'):
    # Add cumulative total to logits.
    cum_logits = list(pd.Series(logits).cumsum())
    total = cum_logits[-1]
    cum_logits.append(total)
    logits.append(total)
    pred_label = 'Prediction'
    pred_label += ' = ' + str(pred_val) if pred_val != '' else ''
    labels.append(pred_label)

    # red for features decreasing probability of target, cyan otherwise.
    bar_colors = [neg_color if l < 0 else pos_color for l in logits]
    bar_colors[-1] = total_color

    # Get bottom location of each bar, it's height, and y location
    #  of each annotation to put on the bar.
    bottoms, heights, annotations = _sizes_and_locations(cum_logits)

    # The steps graphically show the levels.
    step = pd.Series(cum_logits).reset_index(drop=True).repeat(3).shift(1)
    step[1::3] = np.nan  # choose index 1 for every 3 rows

    # Plot reference line, bars, and steps.
    fig, ax = plt.subplots()
    ax.axhline(0.0, color=ref_line_color, zorder=7)
    ax.bar(np.arange(len(heights)), heights, bottom=bottoms, zorder=7,
        linewidth=bar_line_width, color=bar_colors,
        edgecolor=bar_edge_color)
    ax.plot(step.index, step, 'k--', linewidth=1.0, zorder=7)
    ax.set_ylabel('logit')

    # Find the min and max logit values.
    min_logit = min(cum_logits)
    max_logit = max(cum_logits)
    bot = min(min_logit, 0) - 0.5
    top = max(max_logit, 0) + 0.5

    # Set limits and labels.
    ax.set_ylim(bot, top)
    ax.grid(axis='x', zorder=7)
    ax.set_xticks(np.arange(len(labels)))
    ax.set_xticklabels(labels, rotation=45, ha='right', fontsize=8)

    # Put logit values on bars.
    for i, y in enumerate(annotations):
        color = 'k' if i != len(annotations) - 1 else 'white'
        ax.annotate("%.2f" % logits[i], (i, y), ha="center", color=color,
                fontsize=8, zorder=7)

    # Use equal-sized step sizes starting from the reference line.
    step_size = 0.5
    center = None
    if bot == -0.5:
        center = bot
        other_side = top
        mult = 1.0
    else:
        center = top
        other_side = bot
        mult = -1.0

    # Set the logit ticks be equal-sized.
    ax.set_yticks(np.arange(center, other_side, mult * step_size))

    # Generate probabilties in equal-sized steps.
    bot_prob, top_prob = round(_sigmoid(bot), 1), round(_sigmoid(top), 1)
    y_ticklabels = np.arange(bot_prob, top_prob, 0.05)
    y_ticklabels = [y for y in y_ticklabels if (y != 0 and y != 1)]
    y_ticks = [_logodds(y_ticklabel) for y_ticklabel in y_ticklabels]

    # Create second y-axis corresponding to probabilities.
    ax2 = ax.twinx()
    ax2.set_ylim(bot, top)
    ax2.set_yticks(y_ticks)
    ax2.set_yticklabels(y_ticklabels)
    ax2.set_ylabel('probability', va='bottom', rotation=270, zorder=4)

    # ax2.grid(axis='y', zorder=4)
    plt.savefig(fname + '.pdf', format='pdf', bbox_inches='tight',
            facecolor=fig.get_facecolor())
    plt.close()


# private
def _logodds(x):
    if x == 0 or x == 1:
        return 0.5
    return math.log(x / (1 - x))


def _sigmoid(x):
    return 1 / (1 + math.exp(-x))


def _sizes_and_locations(logits):
    bottoms = []
    heights = []
    annotations = []

    for i, logit in enumerate(logits):
        if i == 0 or i == len(logits) - 1:
            height = abs(0 - logit)
            bottom = 0 if logit > 0 else logit
            prev_logit = logit
            annotation = np.mean([0, logit])
        else:
            height = abs(prev_logit - logit)
            bottom = logit if prev_logit > logit else prev_logit
            annotation = np.mean([prev_logit, logit])

        prev_logit = logit
        bottoms.append(bottom)
        heights.append(height)
        annotations.append(annotation)

    return bottoms, heights, annotations


# demo
if __name__ == '__main__':
    labels = ['intercept', 'last_evaluation', 'average_monthly_hours',
            'time_spend_company', 'satisfaction_level', 'number_project',
            'work_accident', 'salary', 'sales', 'promotion_last_5years']
    logits = [1.41, -1.1, 0.98, 0.32, 0.27, -0.24, -0.18, 0.11, -0.07,
            0.02]
    waterfall(logits, labels)

import os
import xgboost as xgb
import matplotlib.pyplot as plt
from . import waterfall as w


# public
def explain_instance(self, x, xgb_clf, feats, targets,
        dir='output/explanations/', fname='explanation'):
    if not os.path.exists(dir):
        os.makedirs(dir)

    # extract the xgb trees
    b = xgb_clf._Booster
    eta = xgb_clf.get_params()['learning_rate']
    tree_lst = _model2table(b, eta=eta)

    # predict the target instance
    sample_m = xgb.DMatrix(x)
    leaf_lst = b.predict(sample_m, pred_leaf=True)
    pred_val = b.predict(sample_m)
    pred_ndx = 0 if pred_val < 0.5 else 1
    prediction = targets[pred_ndx]

    feat_vals = list(x[0])

    # get contributions
    values, labels = [], []
    dist = _logit_contribution(tree_lst, leaf_lst[0])
    for k, v in dist.items():
        if k != 'intercept':
            feat_ndx = int(k[1:])
            feat_val = round(feat_vals[feat_ndx], 4)
            label = feats[feat_ndx]
            label += ' = ' + str(feat_val)
        else:
            label = 'intercept'
        values.append(v)
        labels.append(label)

    # show explanation
    w.waterfall(values, labels, pred_val=prediction, fname=dir + fname)


# show a feature's contribution as that feature's value changes
def feature_contribs(self, xgb_clf, x_test, feats,
        dir='output/contributions/'):
    if not os.path.exists(dir):
        os.makedirs(dir)

    b = xgb_clf._Booster
    eta = xgb_clf.get_params()['learning_rate']
    tree_lst = _model2table(b, eta=eta)
    d_test = xgb.DMatrix(x_test)

    feat_logits = [[] for i in range(len(feats))]
    feat_vals = [[] for i in range(len(feats))]

    # get contributions
    leaf_lsts = b.predict(d_test, pred_leaf=True)
    for i, leaf_lst in enumerate(leaf_lsts):
        dist = _logit_contribution(tree_lst, leaf_lst)

        for j, feat in enumerate(feats):
            feat_logits[j].append(dist.get('f' + str(j), 0))

    # get values
    for x in x_test:
        for j, feat in enumerate(feats):
            feat_vals[j].append(x[j])

    for j, feat in enumerate(feats):
        fig, ax = plt.subplots()
        ax.scatter(feat_vals[j], feat_logits[j])
        ax.set_title(feat)
        ax.set_ylabel('logit contribution')
        ax.set_xlabel('value')
        plt.savefig(dir + feat + '.pdf', format='pdf', bbox_inches='tight')
        plt.close()


# private
def _check_params(tree, eta, lmda):
    result = 0

    right = tree[-1]
    left = tree[-2]
    assert left['is_leaf']
    assert right['is_leaf']
    assert left['parent'] == right['parent']
    parent = tree[left['parent']]

    Hl = left['cover']
    Hr = right['cover']
    Gl = -1. * left['leaf'] * (Hl + lmda) / eta
    Gr = -1. * right['leaf'] * (Hr + lmda) / eta

    Gp = Gl + Gr
    Hp = Hl + Hr
    expect_gain = Gl ** 2 / (Hl + lmda) + Gr ** 2 / (Hr + lmda) - Gp ** 2 / \
        (Hp + lmda)
    if not abs(expect_gain - parent['gain']) < 1.e-2:
        result = 1
    # assert abs(expect_gain - parent['gain']) < 1.e-2
    return result


def _logit_contribution(tree_lst, leaf_lst):
    dist = {'intercept': 0.0}
    for i, leaf in enumerate(leaf_lst):
        tree = tree_lst[i]
        node = tree[leaf]
        parent_idx = node['parent']
        # print(node, parent_idx)
        while True:
            if parent_idx is None:
                dist['intercept'] += node['logit_delta']
                break
            else:
                parent = tree[parent_idx]
                feat = parent['feature']
                if feat not in dist:
                    dist[feat] = 0.0
                dist[feat] += node['logit_delta']
                node = tree[parent_idx]
                parent_idx = node['parent']
    return dist


def _model2table(bst, eta=0.3, lmda=1.0):
    lst_str = bst.get_dump(with_stats=True)
    tree_lst = [[] for _ in lst_str]
    bad_trees = 0

    for i, line in enumerate(lst_str):
        # print(i, line)
        tree_idx = i
        parent = {}
        parent[0] = None
        lst_node_str = line.split('\n')
        node_lst = [{} for _ in range(len(lst_node_str) - 1)]

        for node in lst_node_str:
            node = node.strip()
            # print("fdfdf",len(node))
            if len(node) <= 0:
                continue
            is_leaf = False

            if ":leaf=" in node:
                is_leaf = True
            # print(segs[0], segs[1])
            node_idx = int(node[:node.index(":")])
            # print(node_idx)
            d = {}
            d['tree'] = tree_idx
            d['node'] = node_idx
            d['is_leaf'] = is_leaf

            if not is_leaf:
                segs = node.split(' ')
                fl = node.index('[')
                fr = node.index('<')
                d['feature'] = node[fl + 1:fr]
                for p in segs[1].split(','):
                    k, v = p.split('=')
                    d[k] = v
                d['yes'] = int(d['yes'])
                d['no'] = int(d['no'])
                d['missing'] = int(d['missing'])
                parent[d['yes']] = node_idx
                parent[d['no']] = node_idx
                d['gain'] = float(d['gain'])
                d['cover'] = float(d['cover'])
            else:
                _, lc = node.split(':')
                for p in lc.split(','):
                    k, v = p.split('=')
                    d[k] = v
                d['leaf'] = float(d['leaf'])
                d['cover'] = float(d['cover'])

            # node_lst.append(d)
            node_lst[node_idx] = d
        for j, node in enumerate(node_lst):
            node_lst[j]['parent'] = parent[node_lst[j]['node']]
        tree_lst[i] = node_lst

    for ndx, t in enumerate(tree_lst):
        # print(ndx)
        bad_trees += _check_params(t, eta, lmda)
        for j in reversed(range(len(t))):
            node = t[j]
            if node['is_leaf']:
                G = -1. * node['leaf'] * (node['cover'] + lmda) / eta
            else:
                G = t[node['yes']]['grad'] + t[node['no']]['grad']
            t[j]['grad'] = G
            t[j]['logit'] = -1. * G / (node['cover'] + lmda) * eta

    for t in tree_lst:
        for j in reversed(range(len(t))):
            node = t[j]
            if node['parent'] is None:
                node['logit_delta'] = node['logit'] - .0
            else:
                node['logit_delta'] = node['logit'] -\
                    t[node['parent']]['logit']

    if bad_trees / float(len(tree_lst)) > 0.02:
        print('bad trees: %d out of %d' % (bad_trees, len(tree_lst)))
    return tree_lst

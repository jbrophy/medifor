from .app import run
from .utility import util as ut
from .model import data as d


def combo_exp(dsets=[], img_qualities=[True, False], s='', evaluation='cc',
        model_types=['xgb', 'rf', 'lr1', 'lr2'], algorithms=['base', 'all']):

    dd = d.get_data_dict()

    for train, val, test in dsets:
        for alg in algorithms:
            for model_type in model_types:
                for use_image_quality in img_qualities:

                    # gs_folds = 2 if 'lr' in model_type else 10
                    gs_folds = 10
                    tr = '+'.join([dd[k] for k in train])
                    te = '+'.join([dd[k] for k in test])
                    t_cc = (tr, te, model_type, use_image_quality, alg)
                    t_cv = (tr, model_type, use_image_quality, alg)
                    t = t_cc if evaluation == 'cc' else t_cv
                    ut.out(s % t)

                    run(train_sets=train, val_sets=val, test_sets=test,
                        algorithms=alg, model_type=model_type,
                        evaluation=evaluation,
                        use_image_quality=use_image_quality,
                        gs_folds=gs_folds, param_search='med')

if __name__ == '__main__':
    cc_dsets = [('2', '4', '1'), ('4', '2', '1'), ('24', None, '1'),
            ('1', '4', '2'), ('4', '1', '2'), ('14', None, '2'),
            ('1', '24', '3'), ('2', '14', '3'), ('4', '12', '3'),
            ('12', '4', '3'), ('14', '2', '3'), ('24', '1', '3'),
            ('124', None, '3'), ('1', '2', '4'), ('2', '1', '4'),
            ('12', None, '4')]
    cc_s = '\ntrain: %s,  test: %s,  model_type: %s,  '
    cc_s += 'image_qual: %r,  indicators: %s'

    cv_dsets = [('1', None, '1'), ('2', None, '1'), ('4', None, '1')]
    cv_s = '\ndataset: %s,  model_type: %s,  image_qual: %r,  indicators: %s'

    exp(dsets=cc_dsets, s=cc_s, evaluation='cc')

.. brophy documentation master file, created by
   sphinx-quickstart on Mon Dec 18 11:23:20 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Medifor reasoning engine!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

app\.model\.task module
=======================

.. automodule:: app.model.task
    :members:
    :undoc-members:
    :show-inheritance:

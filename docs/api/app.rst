app package
===========

.. automodule:: app
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    app.interpretability
    app.model
    app.utility

Submodules
----------

.. toctree::

   app.app
   app.experiments


app\.model package
==================

.. automodule:: app.model
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

.. toctree::

   app.model.data
   app.model.task

